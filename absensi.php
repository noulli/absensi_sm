<?php

require_once "koneksi.php";
?>

<!DOCTYPE html>
<html>
<head>
	<link rel="shortcut icon" type="image/png" href="Koala.png"/>
	<title>BETHANY EKKLESIA Kids</title>
	<meta charset="utf-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no" />
	<link rel="stylesheet" href="assets/css/main.css" />
	<script type="text/javascript" language="javascript">
	   function addRow(){
	   	var namaMurid = document.getElementById("murid").value;
	   	if(namaMurid != "")
	   	{
	   		var rows="<tr><td>ID</td><td>"+ namaMurid +"</td><td></td></tr>";
        	$("#applyTable").append(rows);
        	document.getElementById("murid").value = "";
	   	}
	   	else
	   	{
	   		 alert("Nama tidak boleh kosong");
	   	}
	   }
    </script>
</head>
<body class="is-preload">
	<div id="wrapper">
		<div id="main">
			<div class="inner">
			<!-- Content -->
				<header id="header">
					<a href="home.php" class="logo"><strong>Bethany Ekklesia</strong> KIDS</a>
					<ul class="icons">
						<li><a href="#" class="icon fa-twitter"><span class="label">Twitter</span></a></li>
						<li><a href="#" class="icon fa-facebook"><span class="label">Facebook</span></a></li>
						<li><a href="#" class="icon fa-instagram"><span class="label">Instagram</span></a></li>
						<li><a href="index.php" class="button primary">KELUAR</a></li>
					</ul>
				</header>

				<section>
					<h2>Form Absensi</h2>
						<form method="post" action="prosesabsen.php">
						<div>
							<h4>Tanggal :</h4>
							<ul class="actions">
								<li><input type="text" name="tanggal" placeholder="Tanggal" /></li>
								<li><select id="bulan" name="bulan">
										<option value="">Bulan</option>
										<option value="1">Januari</option>
										<option value="2">Pebruari</option>
										<option value="3">Maret</option>
										<option value="4">April</option>
										<option value="5">Mei</option>
										<option value="6">Juni</option>
										<option value="7">Juli</option>
										<option value="8">Agustus</option>
										<option value="9">September</option>
										<option value="10">Oktober</option>
										<option value="11">Nopember</option>
										<option value="12">Desember</option>
									</select></li>
								<li><input id="tanggal" type="text" name="tahun" placeholder="Tahun" /></li>
							</ul>

							<ul class="actions">
								<li><h4>Sesi :</h4></li>
								<li><select nama="sesi">
									<option value="">Ibadah</option>
									<option value="1">Pagi</option>
									<option value="2">Siang</option>
									<option value="3">Sore</option>
								</select></li>
							</ul>

							<h4>Nama Murid : </h4>
							<ul class="actions">
								<li><input type="text" id="murid" placeholder="Masukkan Nama Murid" /></li>
								<!-- <li><button name="tambah" class="button primary">Tambahkan</button></li> -->
								<li><input type="button" value="Hadir" onclick="addRow()" /></li>
							</ul>
								<!-- Break -->
							<h4>Murid yang masuk</h4>
							<div class="table-wrapper">
								<table>
									<thead>
										<tr>
											<th>ID</th>
											<th>Nama</th>
											<th>Batal</th>
										</tr>
									</thead>
									<tbody id="applyTable">
									<tr>
																	<td>12</td>
																	<td>Ante turpis integer aliquet porttitor.</td>
																	<td><a href="#" class="icon fa-remove"></a></td>
																</tr>
									</tbody>
								</table>
							</div>
								<!-- Break -->
								<div class="col-12">
									<ul class="actions">
										<li><input type="submit" value="Send Message" class="primary" /></li>
										<li><input type="reset" value="Reset" /></li>
									</ul>
								</div>
							</div>
						</form>
					</section>

    		</div>
    	</div>
    </div>

    	<!-- Sidebar -->
		<div id="sidebar">
			<div class="inner">

				<!-- Menu -->
				<nav id="menu">
					<header class="major">
						<h2>Menu</h2>
					</header>
					<ul>
						<li><a href="home.php">Home</a></li>
						<li><a href="absensi.php">Absensi</a></li>
						<li>
							<span class="opener">Data Murid</span>
							<ul>
								<li><a href="#">Tambah Murid</a></li>
								<li><a href="#">Update/Hapus Murid</a></li>
							</ul>
						</li>
						<li>
							<span class="opener">Data Guru</span>
							<ul>
								<li><a href="#">Tambah Guru</a></li>
								<li><a href="#">Update/Hapus Guru</a></li>
							</ul>
						</li>
						<li><a href="index.php">KELUAR</a></li>
					</ul>
				</nav>

				<!-- Section -->
				<section>
					<header class="major">
						<h2>Get in touch</h2>
					</header>
					<p>Sed varius enim lorem ullamcorper dolore aliquam aenean ornare velit lacus, ac varius enim lorem ullamcorper dolore. Proin sed aliquam facilisis ante interdum. Sed nulla amet lorem feugiat tempus aliquam.</p>
					<ul class="contact">
						<li class="fa-envelope-o"><a href="#">information@untitled.tld</a></li>
						<li class="fa-phone">(000) 000-0000</li>
						<li class="fa-home">1234 Somewhere Road #8254<br />
						Nashville, TN 00000-0000</li>
					</ul>
				</section>

			<!-- Footer -->
				<footer id="footer">
					<p class="copyright">&copy; <a href="index.php">Bethany Ekklesia KIDS</a>.</p>
				</footer>

			</div>
		</div>

    </div>


        <!-- Scripts -->
        <script src="assets/js/jquery.min.js"></script>
		<script src="assets/js/browser.min.js"></script>
		<script src="assets/js/breakpoints.min.js"></script>
		<script src="assets/js/util.js"></script>
		<script src="assets/js/main.js"></script>
</body>
</html>