<?php
   include('koneksi.php');
   include ('session.php');
?>

<!DOCTYPE html>
<html>
<head>
	<link rel="shortcut icon" type="image/png" href="Koala.png"/>
	<title>BETHANY EKKLESIA Kids</title>
	<meta charset="utf-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no" />
	<link rel="stylesheet" href="assets/css/main.css" />
</head>
<body class="is-preload">
	<div id="wrapper">
		<div id="main">
			<div class="inner">
			<!-- Content -->
				<header id="header">
					<a href="home.php" class="logo"><strong>Bethany Ekklesia</strong> KIDS</a>
					<ul class="icons">
						<li><a href="#" class="icon fa-twitter"><span class="label">Twitter</span></a></li>
						<li><a href="#" class="icon fa-facebook"><span class="label">Facebook</span></a></li>
						<li><a href="#" class="icon fa-instagram"><span class="label">Instagram</span></a></li>
						<li><a href="logout.php" class="button primary">KELUAR</a></li>
					</ul>
				</header>

				<section>
					<header class="major">
						<h2>Selamat Datang,
							<?php echo "$login_session"?>
							!
						</h2>
						<div>
						<?php
						echo '<img src="data:image/jpeg;base64,'.base64_encode($foto_session).'"style="width:150px">';
			          	?>
			          	</div>
						<p>Ada apa hari ini?</p>

					</header>
					<div class="features">
						<article>
							<a href="absensi.php" class="icon fa-calendar"></a>
							<a href="absensi.php" class="logo">
								<div class="content">
									<h3>Absensi Sekolah Minggu </h3>
								</div>
							</a>
						</article>
						<article>
							<a href="index.php" class="icon fa-users"></a>
							<a href="index.php" class="logo">
								<div class="content">
									<h3>Data Murid</h3>
								</div>
							</a>
						</article>
						<article>
							<a href="index.php" class="icon fa-briefcase"></a>
							<a href="index.php" class="logo">
								<div class="content">
									<h3>Data Guru</h3>
								</div>
							</a>
						</article>
						<article>
							<a href="index.php" class="icon fa-book"></a>
							<a href="index.php" class="logo">
								<div class="content">
									<h3>Tidak apa-apa</h3>
									<p>Gak tau mau ngapain.</p>
								</div>
							</a>
						</article>
					</div>
				</section>

    		</div>
    	</div>
    </div>

        <!-- Scripts -->
        <script src="assets/js/jquery.min.js"></script>
		<script src="assets/js/browser.min.js"></script>
		<script src="assets/js/breakpoints.min.js"></script>
		<script src="assets/js/util.js"></script>
		<script src="assets/js/main.js"></script>
</body>
</html>