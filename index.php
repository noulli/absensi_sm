<?php
   include("koneksi.php");
   session_start();
   if(isset($_POST['masuk']))
   {
      // username and password sent from form 
      
      $myusername = mysqli_real_escape_string($db,$_POST['username']);
      $mypassword = mysqli_real_escape_string($db,$_POST['password']);
      
      $sql = "SELECT count(*) active FROM guru WHERE username = '$myusername' and password = '$mypassword'";

      $result = mysqli_query($db,$sql);
      $row = mysqli_fetch_array($result);

      $count = $row['active'];
      
      // If result matched $myusername and $mypassword, table row must be 1 row
        
      if($count == 1)
      {
         $_SESSION['login_user'] = $myusername;
         header("location:home.php");
      }
      else
      {
            echo '<script language="javascript"> 
						        alert("Username/Password Anda SALAH")
						        document.location.href="index.php"
						        </script>';
      }
   }
?>

<!DOCTYPE html>
<html>
<head>
	<link rel="shortcut icon" type="image/png" href="Koala.png"/>
	<title>BETHANY EKKLESIA Kids</title>
	<meta charset="utf-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no" />
	<link rel="stylesheet" href="assets/css/main.css" />
</head>
<body class="is-preload">
	<div id="wrapper">
		<div id="main">
			<div class="inner">
			<!-- Content -->
				<header id="header">
					<a href="index.php" class="logo"><strong>Bethany Ekklesia</strong> KIDS</a>
				</header>

				<section>
				<h3>LOGIN FORM</h3>
					<form method="post" action="">
						<div class="row gtr-uniform">
							<div class="col-6 col-12">
								<input type="text" name="username" id="demo-name" value="" placeholder="Username" />
							</div>
							<div class="col-6 col-12">
								<input type="password" name="password" id="demo-email" value="" placeholder="Password" />
							</div>
							<!-- Break -->
							<div class="col-12">
								<input type="submit" name="masuk" value="MASUK" class="primary" />
							</div>
						</div>
					</form>
				</section>

    		</div>
    	</div>
    </div>

        <!-- Scripts -->
        <script src="assets/js/jquery.min.js"></script>
		<script src="assets/js/browser.min.js"></script>
		<script src="assets/js/breakpoints.min.js"></script>
		<script src="assets/js/util.js"></script>
		<script src="assets/js/main.js"></script>
</body>
</html>